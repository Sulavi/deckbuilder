#ifndef NETWORKINTERFACE_H
#define NETWORKINTERFACE_H

#include <QObject>
#include <QAbstractSocket>

#include "deckbuilder.pb.h"

class NetworkInterface : public QObject
{
    Q_OBJECT

protected:
    union MessageHeader{
        qint32 size;
        char buf[4];
    };

public:
    NetworkInterface(QObject* parent = nullptr) : QObject(parent){};
    virtual ~NetworkInterface(){}

    virtual bool Connect(QString serverIP, uint serverPort) = 0;
    virtual void WriteProto(const db::DBMessage & msg) = 0;

signals:
    void ReadReady();
    void Connected();
    void Disconnected();
    void Error(QAbstractSocket::SocketError error);
};

#endif // NETWORKINTERFACE_H
