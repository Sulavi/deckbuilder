#include "networkserver.h"

NetworkServer::NetworkServer(QObject *parent)
    : NetworkInterface(parent)
{
    connect(&server, &QTcpServer::newConnection, this, &NetworkServer::newConnection);
}

bool NetworkServer::Connect(QString serverIP, uint serverPort){
    Listen(serverPort);
    return true;
}

void NetworkServer::Listen(uint port){
    server.listen(QHostAddress::Any, port);
}

void NetworkServer::Close(){
    server.close();
}

void NetworkServer::Disconnect(){
    for(auto i=0; i<clients.size(); i++){
        if(clients.at(i)->state() != QTcpSocket::ConnectedState){
            clients.at(i)->disconnectFromHost();
            clients.at(i)->deleteLater();
        }
    }

    clients.clear();
}

void NetworkServer::newConnection(){
    QTcpSocket * newClient = server.nextPendingConnection();
    clients.push_back(newClient);
    connect(newClient, &QTcpSocket::disconnected, this,  &NetworkServer::cleanConnections);
    connect(newClient, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this,  &NetworkServer::clientError);
    connect(newClient, &QTcpSocket::readyRead, this, &NetworkServer::readFromSender);
}

void NetworkServer::cleanConnections(){
    QVector<int> indexes;
    for(auto i=0; i<clients.size(); i++){
        if(clients.at(i)->state() != QTcpSocket::ConnectedState){
            indexes.push_front(i);
        }
    }

    for(auto i=0; i<indexes.size(); i++){
        clients.at(i)->deleteLater();
        clients.removeAt(i);
    }
}

void NetworkServer::clientError(QAbstractSocket::SocketError error){
    //TODO: print error
    cleanConnections();
}

void NetworkServer::write(QTcpSocket * socket, QByteArray msg){
    MessageHeader header;
    header.size = msg.size();
    msg.prepend(QByteArray::fromRawData(header.buf, 4));
    socket->write(msg);
}

void NetworkServer::readFromSender(){
    QTcpSocket * socket = qobject_cast<QTcpSocket*>(sender());

    MessageHeader header;
    qint64 read = socket->read(header.buf, 4);
    if(read != 4){
        throw std::runtime_error("Failed to read message header");
    }

    qint64 totalRead = 0;
    char* buf = (char*)malloc(header.size);
    while(totalRead < header.size){
        read = socket->read(buf + totalRead, header.size);
        if(read == -1){
            free(buf);
            throw std::runtime_error("Failed to read message");
        }
        totalRead += read;
    }

    db::DBMessage msg;
    msg.ParseFromArray(buf, header.size);
    msgBuffer.push_back(msg);

    free(buf);

    emit ReadReady();
}

void NetworkServer::WriteProto(const db::DBMessage & msg){
    QByteArray proto = QByteArray::fromStdString(msg.SerializeAsString());
    for(auto it=clients.begin(); it!=clients.end(); it++){
        write(*it, proto);
    }
}

db::DBMessage NetworkServer::ReadProto(){
    db::DBMessage msg;
    if(msgBuffer.size() > 0){
        msg = msgBuffer.at(0);
        msgBuffer.removeFirst();
        return msg;
    } else {
        throw std::runtime_error("No Messages buffered");
    }
}
