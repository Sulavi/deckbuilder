#include "networkclient.h"

NetworkClient::NetworkClient(QObject *parent)
    : QObject(parent)
{
    connect(&socket, &QTcpSocket::connected, this, &NetworkClient::Connected);
    connect(&socket, &QTcpSocket::disconnected, this, &NetworkClient::Disconnected);
    connect(&socket, &QTcpSocket::readyRead, this, &NetworkClient::ReadReady);
    connect(&socket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this, &NetworkClient::Error);
}

NetworkClient::~NetworkClient(){}

bool NetworkClient::Connect(QString serverIP, uint serverPort){
    socket.connectToHost(serverIP, serverPort);
//    if(blocking){
//        if(!socket.waitForConnected(10000)){
//            socket.disconnectFromHost();
//            return false;
//        }
//    }
    return true;
}

void NetworkClient::Disconnect(){
    socket.disconnectFromHost();
}

void NetworkClient::write(QByteArray msg){
    MessageHeader header;
    header.size = msg.size();
    msg.prepend(QByteArray::fromRawData(header.buf, 4));
    socket.write(msg);
}

QByteArray NetworkClient::read(){
    MessageHeader header;
    qint64 read = socket.read(header.buf, 4);
    if(read != 4){
        throw std::runtime_error("Failed to read message header");
    }

    qint64 totalRead = 0;
    char* buf = (char*)malloc(header.size);
    while(totalRead < header.size){
        read = socket.read(buf + totalRead, header.size);
        if(read == -1){
            free(buf);
            throw std::runtime_error("Failed to read message");
        }
        totalRead += read;
    }

    QByteArray msg(buf, header.size);
    free(buf);

    return msg;
}

void NetworkClient::WriteProto(const db::DBMessage & msg){
    QByteArray proto = QByteArray::fromStdString(msg.SerializeAsString());
    write(proto);
}

db::DBMessage NetworkClient::ReadProto(){
    QByteArray msg = read();
    db::DBMessage proto;
    bool rc = proto.ParseFromArray(msg.data(), msg.size());
    if(!rc){
        throw std::runtime_error("Failed to parse proto message");
    }
    return proto;
}

void NetworkClient::RequestGameDescription(){
    db::DBMessage msg;
    msg.mutable_request()->set_request(true);
    WriteProto(msg);
}
