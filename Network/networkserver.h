#ifndef NETWORKSERVER_H
#define NETWORKSERVER_H

#include <QObject>
#include <QVector>
#include <QTcpServer>
#include <QTcpSocket>

#include "Network/networkinterface.h"
#include "deckbuilder.pb.h"

class NetworkServer : public NetworkInterface
{
    Q_OBJECT

    union MessageHeader{
        qint32 size;
        char buf[4];
    };

private:
    QTcpServer server;
    QList<QTcpSocket*> clients;

    QVector<db::DBMessage> msgBuffer;

public:
    explicit NetworkServer(QObject *parent = nullptr);

private slots:
    void newConnection();
    void cleanConnections();
    void clientError(QAbstractSocket::SocketError err);

    void write(QTcpSocket * socket, QByteArray msg);
    void readFromSender();

public slots:
    void Listen(uint port);
    void Close();                                           // Stop Listening for new connections
    void Disconnect();                                      // Disconnects all clients

    bool Connect(QString serverIP, uint serverPort) override;
    void WriteProto(const db::DBMessage & msg) override;
    db::DBMessage ReadProto();

signals:
    void ReadReady();

};

#endif // NETWORKSERVER_H
