#ifndef NETWORKCLIENT_H
#define NETWORKCLIENT_H

#include <QObject>
#include <QAbstractSocket>
#include <QTcpSocket>
#include <QTcpServer>
#include <QMessageBox>

#include "deckbuilder.pb.h"
#include "Network/networkinterface.h"

class NetworkClient : public QObject
{
    Q_OBJECT

    union MessageHeader{
        qint32 size;
        char buf[4];
    };

private:
    QTcpSocket socket;

    void write(QByteArray msg);
    QByteArray read();

public:
    explicit NetworkClient(QObject *parent = nullptr);
    ~NetworkClient();

public slots:
    bool Connect(QString serverIP, uint serverPort);
    void Disconnect();

    void WriteProto(const db::DBMessage & msg);
    db::DBMessage ReadProto();
    void RequestGameDescription();

signals:
    void ReadReady();
    void Connected();
    void Disconnected();
    void Error(QAbstractSocket::SocketError error);
};

#endif // NETWORKCLIENT_H
