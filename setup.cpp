#include "setup.h"
#include "ui_setup.h"

Setup::Setup(QWidget *parent, DeckConfig * config)
    : QWidget(parent)
    , ui(new Ui::Setup)
    , config(config)
{
    ui->setupUi(this);
    this->setWindowTitle("Deck Builder Setup");

    modeChanged(ui->modeCombo->currentIndex());
    on_gameCombo_currentTextChanged(ui->gameCombo->currentText());

    connect(ui->modeCombo, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &Setup::modeChanged);
}

void Setup::modeChanged(int newMode){
    MODE mode = static_cast<MODE>(newMode);
    switch (mode) {
    case LOCAL:
        ui->serverWidget->setVisible(false);
        ui->clientWidget->setVisible(false);
        ui->gameWidget->setVisible(true);
        break;
    case SERVER:
        ui->serverWidget->setVisible(true);
        ui->clientWidget->setVisible(false);
        ui->gameWidget->setVisible(true);
        break;
    case CLIENT:
        ui->serverWidget->setVisible(false);
        ui->clientWidget->setVisible(true);
        ui->gameWidget->setVisible(true);
        break;
    default:
        errorPopup("Invalid mode in modeChanged");
    }
}

void Setup::on_gameCombo_currentTextChanged(const QString & newText)
{
    if(newText.compare("Custom") == 0){
        ui->gamePath->setEnabled(true);
        ui->browseButton->setEnabled(true);
    } else {
        ui->gamePath->setEnabled(false);
        ui->browseButton->setEnabled(false);
    }
}

void Setup::on_startButton_clicked()
{
    MODE mode = static_cast<MODE>(ui->modeCombo->currentIndex());
    config->mode = mode;

    if(mode == SERVER){
        config->port = ui->portSpin->value();
    } else if(mode == CLIENT) {
        if(!evalNetwork(ui->serverIPLine->text(), ui->serverPort->value())){
            errorPopup("Unable to connect to " + ui->serverIPLine->text() + ":" + ui->serverPort->text());
            return;
        }
        config->ip = ui->serverIPLine->text().toStdString();
        config->port = ui->serverPort->value();
    }

    if(ui->gameCombo->currentText().compare("Hollow Knight") == 0){
        config->configPath = "/home/richard/QT Projects/DeckBuilderProject/DeckBuilder/Schemas/HollowKnight.json";
    } else if(ui->gameCombo->currentText().compare("Legendary") == 0){
        config->configPath = "/home/richard/QT Projects/DeckBuilderProject/DeckBuilder/Schemas/Legendary.json";
    } else if(ui->gameCombo->currentText().compare("Custom") == 0){
        config->configPath = ui->gamePath->text().toStdString();
    } else {
        errorPopup("Richard done fucked up.");
    }
    if(!evalPath(QString::fromStdString(config->configPath))){
        errorPopup("Invalid path " + QString::fromStdString(config->configPath));
        return;
    }

    config->useCache = ui->cacheCheck->isChecked();
    config->cachePath = ui->cachePathEdit->text().toStdString();
    config->decksPath = ui->decksPathEdit->text().toStdString();

    config->valid = true;
    close();
}

bool Setup::evalPath(QString configPath){
    return true;
    return QFileInfo::exists(configPath);
}

bool Setup::evalNetwork(QString IP, qint64 port){
    QTcpSocket testSocket;
    testSocket.connectToHost(IP, port);
    if(!testSocket.waitForConnected(5000)){
        testSocket.abort();
        return false;
    }
    testSocket.disconnectFromHost();
    return true;
}

void Setup::errorPopup(QString msg){
    QMessageBox::warning(this, "Error", msg);
}

