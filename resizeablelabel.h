#ifndef RESIZEABLELABEL_H
#define RESIZEABLELABEL_H

#include <QWidget>
#include <QLabel>

class ResizeableLabel : public QLabel
{
    Q_OBJECT
public:
    ResizeableLabel(QWidget* parent);

    void resizeEvent(QResizeEvent *event) override;

    void setLandscape(bool landscape);

private:
    float aspectRatio = 0.719; // w/h
};

#endif // RESIZEABLELABEL_H
