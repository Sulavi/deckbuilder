#include "mainwindow.h"
#include "gametable.h"
#include "setup.h"
#include "deckconfig.hpp"

#include <QApplication>

int main(int argc, char *argv[])
{
    DeckConfig config;

    QApplication configApp(argc, argv);

    Setup s(nullptr, &config);
    s.show();
    int rc = configApp.exec();
    configApp.quit();

    if(!config.valid){
        exit(rc);
    }

//    config.configPath = "/home/richard/QT Projects/DeckBuilderProject/DeckBuilder/Schemas/HollowKnight.json";
//    config.valid = true;
//    config.mode = MODE::LOCAL;

    QApplication gameApp(argc, argv);
    GameTable table(&config);
    table.show();
    return gameApp.exec();
}
