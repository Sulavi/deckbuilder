#include "cardlistmodel.h"

CardListModel::CardListModel(QObject *parent)
    : QAbstractListModel(parent)
{
    cardlist = new QList<Card*>();
}

void CardListModel::addCard(Card *card){
    beginInsertRows(QModelIndex(), cardlist->size(), cardlist->size());
    cardlist->append(card);
    endInsertRows();
}

void CardListModel::removeCharacter(QString characterName){
    int i=0;
    while(i < cardlist->size()){
        if(cardlist->at(i)->getHero() == characterName){
            beginRemoveRows(QModelIndex(), i, i);
            cardlist->removeAt(i);
            endRemoveRows();
        } else {
            i++;
        }
    }
}

int CardListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || cardlist == nullptr)
        return 0;

    return cardlist->size();
}

QVariant CardListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() ||
        index.row() >= cardlist->size())
    {
        return QVariant();
    }

    if(role == Qt::DisplayRole){
        return cardlist->at(index.row())->getName();
    } else if(role == Qt::UserRole){
        return cardlist->at(index.row())->getPixmap(false);
    }

    return QVariant();
}

Card* CardListModel::getCard(const QModelIndex &index){
    if (!index.isValid() ||
        index.row() >= cardlist->size())
    {
        return nullptr;
    }

    return cardlist->at(index.row());
}

Card* CardListModel::getCardByName(QString cardname){
    for(int i=0; i<cardlist->size(); i++){
        if(cardlist->at(i)->getName() == cardname){
            return cardlist->at(i);
        }
    }

    return nullptr;
}
