#ifndef STORE_H
#define STORE_H

#include <QDialog>
#include <QRegularExpression>
#include <QMenu>
#include <QMessageBox>
#include "card.h"

namespace Ui {
class Store;
}

class Store : public QDialog
{
    Q_OBJECT

public:
    explicit Store(QWidget *parent = nullptr);
    ~Store();

private:
    Ui::Store *ui;
    QMenu* contextMenu;

    QList<Card*>* cardList;

    int recruit = 0;
    bool limit = true;

    int contextCardIndex;
    void customContextMenuRequested(QPoint);

    void updateStore();
    //Card* takeCard(int index);

    void buy(int index);
    void remove(int index);
    void popout(int index);
    void clicked();

signals:
    void CardBought(Card* card);
    void Popout(Card* card);

public slots:
    void setLimitRecruit(bool limit);
    bool addCard(Card* card);
    void recruitChanged(int recruit);
    void clear();
};

#endif // STORE_H
