#ifndef CARDFETCHER_H
#define CARDFETCHER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QPixmap>
#include <QList>
#include <QNetworkReply>
#include <QDir>

#include "card.h"

class CardFetcher : public QObject
{
    Q_OBJECT
public:
    explicit CardFetcher(QObject *parent = nullptr);

private:
    QNetworkAccessManager* manager;

    QList<Card*> cardQueue;

    bool useCache;
    QString cache;

public slots:
    void cardDownloaded(QNetworkReply* reply);
    void getCard(Card* card);

    void setCache(bool useCache);
    void setCachePath(QString cachePath);

signals:

};

#endif // CARDFETCHER_H
