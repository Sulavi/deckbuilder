#ifndef SETUP_H
#define SETUP_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QComboBox>
#include <QMessageBox>
#include <QFileInfo>
#include <QHostInfo>

#include "deckconfig.hpp"
#include "Network/networkclient.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Setup; }
QT_END_NAMESPACE

class Setup : public QWidget
{
    Q_OBJECT

private:
    Ui::Setup *ui;

    DeckConfig * config;

public:
    explicit Setup(QWidget *parent = nullptr, DeckConfig * config = nullptr);

public slots:
     void modeChanged(int newMode);
private slots:
     void on_gameCombo_currentTextChanged(const QString &arg1);
     void on_startButton_clicked();

     void errorPopup(QString msg);
     bool evalPath(QString configPath);
     bool evalNetwork(QString IP, qint64  port);
};

#endif // SETUP_H
