#ifndef RESIZEABLEBUTTON_H
#define RESIZEABLEBUTTON_H

#include <QPushButton>

class ResizeableButton : public QPushButton
{
    Q_OBJECT
public:
    ResizeableButton(QWidget* parent = nullptr);

    void resizeEvent(QResizeEvent* event) override;

    void setLandscape(bool landscape);

private:
    float aspectRatio = 0.719; // w/h
};

#endif // RESIZEABLEBUTTON_H
