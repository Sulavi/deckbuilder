#ifndef LOADFILE_H
#define LOADFILE_H

#include <QObject>
#include <QDialog>
#include <QPushButton>
#include <QFileDialog>
#include <QStandardPaths>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QMessageBox>
#include <QTreeWidget>
#include <QRegularExpression>
#include "card.h"
#include "cardfetcher.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Dialog; }
QT_END_NAMESPACE

class LoadFile : public QDialog
{
    Q_OBJECT

public:
    LoadFile(QWidget* parent = nullptr);
    ~LoadFile();

private:
    Ui::Dialog *ui;
    CardFetcher cardFetcher;

    QList<Card*> cardList;

    QStringList loadedCharacters;
    QStringList checkedCharacters;

    void openJsonBrowser();
    void openCacheBrowser();

signals:
    void CardLoaded(Card* card);
    void CharacterUnchecked(QString characterName);

private slots:
    void loadDirectory(QString directory);
    void loadFile(QString filename);

    void itemClicked(QTreeWidgetItem* item);
};

#endif // LOADFILE_H
