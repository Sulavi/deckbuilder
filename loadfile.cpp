#include "loadfile.h"
#include "ui_loadfile.h"

LoadFile::LoadFile(QWidget* parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
{
    ui->setupUi(this);

    connect(ui->browseButton, &QPushButton::clicked, this, &LoadFile::openJsonBrowser);
    connect(ui->cacheBrowseButton, &QPushButton::clicked, this, &LoadFile::openCacheBrowser);
    connect(ui->treeWidget, &QTreeWidget::itemClicked, this, &LoadFile::itemClicked);
    connect(ui->lineEdit, &QLineEdit::textChanged, this, &LoadFile::loadDirectory);
    connect(ui->cacheCheck, &QCheckBox::stateChanged, [=](){
        cardFetcher.setCache(ui->cacheCheck->isChecked());
    });
    connect(ui->cacheEdit, &QLineEdit::textChanged, [=](QString newPath){
        cardFetcher.setCachePath(newPath);
    });


    QDir cacheDir(QDir::currentPath() + "/Cache");
    if(cacheDir.exists()){
        ui->cacheEdit->setText(cacheDir.path());
        cardFetcher.setCachePath(cacheDir.path());
    }
    cardFetcher.setCache(ui->cacheCheck->isChecked());

    QDir jsonDir(QDir::currentPath() + "/Json");
    if(jsonDir.exists()){
        ui->lineEdit->setText(jsonDir.path());
    }

}

LoadFile::~LoadFile()
{
    for(auto cardIt=cardList.constBegin(); cardIt!=cardList.constEnd(); cardIt++){
        delete *cardIt;
    }
    delete ui;
}

void LoadFile::openJsonBrowser(){
    QString filename = QFileDialog::getExistingDirectory(this, tr("Open JSON Directory"),
                                                        ui->lineEdit->text(),
                                                        QFileDialog::DontResolveSymlinks);

    if(!filename.isNull()){
        ui->lineEdit->setText(filename);
    }
}

void LoadFile::openCacheBrowser(){
    QString filename = QFileDialog::getExistingDirectory(this, tr("Open Cache Directory"),
                                                    ui->cacheEdit->text(),
                                                    QFileDialog::DontResolveSymlinks);
    if(!filename.isNull()){
        ui->cacheEdit->setText(filename);
    }
}

void LoadFile::loadDirectory(QString directory){
    QDir jsonDir(directory);
    if(jsonDir.exists()){
        QStringList files = jsonDir.entryList({"*.json"});
        for(int i=0; i<files.size(); i++){
            qDebug() << "Loading file: " << files[i];
            loadFile(directory + "/" + files[i]);
        }
    }
}

void LoadFile::loadFile(QString filename){
    QFile file(filename);
    if(!file.exists()){
        return;
    }

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        return;
    }

    //QRegularExpression re("Legendary_(.*)\\.json");
    QRegularExpression re("Legendary_(.*)");
    QString basename = QFileInfo(filename).baseName();
    QRegularExpressionMatch match = re.match(basename);

    if(!match.hasMatch()){
        return;
    }

    QString setName = match.captured(1).replace('_', ' ');

    QTreeWidgetItem* setItem;
    QList<QTreeWidgetItem*> headerItems = ui->treeWidget->findItems(setName, Qt::MatchExactly);
    if(headerItems.size() > 0){
        setItem = headerItems.at(0);
    } else {
        setItem = new QTreeWidgetItem(ui->treeWidget, {setName});
    }

    QJsonDocument jsonDoc = QJsonDocument::fromJson(file.readAll());
    file.close();

    QJsonArray characters = jsonDoc.object().value("characters").toArray();

    for(auto characterIt=characters.constBegin(); characterIt!=characters.constEnd(); characterIt++){
        QJsonObject character = characterIt->toObject();

        QString characterName = character.keys().at(0);
        if(loadedCharacters.contains(characterName)){
            continue;
        }
        loadedCharacters.append(characterName);
        QTreeWidgetItem* newTreeItem = new QTreeWidgetItem(setItem, {characterName});
        newTreeItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
        newTreeItem->setCheckState(0, Qt::Unchecked);

        QJsonArray cards = character.value(characterName).toArray();

        for(auto cardIt=cards.constBegin(); cardIt!=cards.constEnd(); cardIt++){
            QJsonObject card = cardIt->toObject();

            bool split = false;
            if(card.contains("split")){
                split = card.value("split").toBool();
            }

            Card* newCard = new Card(card.value("name").toString(),
                                     characterName,
                                     card.value("team").toString(),
                                     card.value("class").toString(),
                                     card.value("cost").toInt(),
                                     card.value("attack").toInt(),
                                     card.value("recruit").toInt(),
                                     card.value("url").toString(),
                                     card.value("text").toString(),
                                     split,
                                     this);

            cardList.append(newCard);
        }
    }

}

void LoadFile::itemClicked(QTreeWidgetItem* item){
    if(item->checkState(0) == Qt::Checked){
        if(!checkedCharacters.contains(item->text(0))){
            checkedCharacters.append(item->text(0));
            for(auto it=cardList.constBegin(); it!=cardList.constEnd(); it++){
                if((*it)->getHero() == item->text(0)){
                    cardFetcher.getCard(*it);
                    emit CardLoaded((*it));
                }
            }
        }
    } else {
        if(checkedCharacters.contains(item->text(0))){
            checkedCharacters.removeAt(checkedCharacters.indexOf(item->text(0)));
            emit CharacterUnchecked(item->text(0));
        }
    }
}
