#include "cardfetcher.h"

CardFetcher::CardFetcher(QObject *parent) : QObject(parent)
{
    manager = new QNetworkAccessManager(this);
    connect(manager, &QNetworkAccessManager::finished, this, &CardFetcher::cardDownloaded);
}

void CardFetcher::getCard(Card* card){

    if(useCache){
        QString folder = card->getHero().replace(' ', '_');
        QString filename = card->getName().replace(' ', '_').replace('/', "_SLASH_").append(".jpg");
        QFile cardFile(cache + "/" + folder + "/" + filename);

        if(cardFile.exists()){
            cardFile.open(QIODevice::ReadOnly);
            QByteArray jpegData = cardFile.readAll();
            cardFile.close();
            QPixmap pixmap;
            pixmap.loadFromData(jpegData);
            card->setPixmap(pixmap);
            return;
        }
    }

    QUrl url = card->getUrl();
    QNetworkRequest request(url);
    manager->get(request);
    cardQueue.append(card);
}

void CardFetcher::cardDownloaded(QNetworkReply* reply){
    QByteArray jpegData = reply->readAll();
    QPixmap pixmap;

    QUrl requestUrl = reply->request().url();

    Card* card;
    for(int i=0; i<cardQueue.size(); i++){
        if(cardQueue.at(i)->getUrl() == requestUrl){
            card = cardQueue.at(i);
            cardQueue.removeAt(i);
        }
    }

    if(pixmap.loadFromData(jpegData)){
        card->setPixmap(pixmap);
    } else {
        qDebug() << "Failed to load picture for " << card->getName();
    }

    if(useCache){
        QString folder = card->getHero().replace(' ', '_');
        if(!QDir(cache + "/" + folder).exists()){
            QDir().mkdir(cache + "/" + folder);
        }
        QString filename = card->getName().replace(' ', '_').replace('/', "_SLASH_").append(".jpg");
        QFile cardFile(cache + "/" + folder + "/" + filename);
        if(!cardFile.exists()){
            bool open = cardFile.open(QIODevice::WriteOnly);
            if(!open){
                qDebug() << cardFile.errorString();
            }
            pixmap.save(&cardFile, "JPG");
            cardFile.close();
        }
    }
}

void CardFetcher::setCache(bool useCache){
    this->useCache = useCache;
}

void CardFetcher::setCachePath(QString cachePath){
    this->cache = cachePath;
}
