#include "card.h"

Card::Card(QString name,
           QString hero,
           QString team,
           QString cardclass,
           int cost,
           int attack,
           int recruit,
           QString url,
           QString text,
           bool split,
           QObject *parent) : QObject(parent)
{
    cardname = name;
    heroname = hero;
    this->cost = cost;
    this->attack = attack;
    this->recruit = recruit;
    this->text = text;
    this->url = QUrl(url);
    this->team = team;
    this->cardClass = cardclass;
    this->split = split;
}

QPixmap Card::getPixmap(bool scaled){
    if(scaled){
        return pixmap.scaled(128, 178, Qt::AspectRatioMode::KeepAspectRatio, Qt::SmoothTransformation);
    } else {
        return pixmap;
    }
}

QUrl Card::getUrl(){ return url; }

QString Card::getName(){ return cardname; }

QString Card::getHero(){ return heroname; }

int Card::getCost(){ return cost; }

int Card::getAttack(){ return attack; }

int Card::getRecruit(){ return recruit; }

QString Card::getText(){ return text; }

QString Card::getTeam(){ return team; }

QString Card::getClass(){ return cardClass; }

bool Card::getSplit(){ return split; }

void Card::setPixmap(QPixmap pixmap){
    this->pixmap = pixmap;
    emit pictureUpdated();
}


