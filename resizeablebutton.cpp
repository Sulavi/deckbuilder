#include "resizeablebutton.h"

ResizeableButton::ResizeableButton(QWidget* parent)
    : QPushButton(parent)
{
//    setBaseSize(128, 178);
//    setSizeIncrement(10, 14);
    setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum));
    setMaximumSize(QSize(1280, 1780));
    setMinimumSize(QSize(128, 178));
}

void ResizeableButton::resizeEvent(QResizeEvent* event){
    int w = width();
    int h = height();

    int maxHeight = w/aspectRatio;
    int maxWidth = h*aspectRatio;

    if(h > maxHeight+1){
        resize(w, maxHeight);
        this->setIconSize(QSize(w, maxHeight));
    } else if(w > maxWidth+1){
        resize(maxWidth, h);
        this->setIconSize(QSize(maxWidth, h));
    }
    this->setIconSize(QSize(width(), height()));
}

void ResizeableButton::setLandscape(bool landscape){
    if(landscape){
        aspectRatio = 1.39;
    } else {
        aspectRatio = 0.719;
    }
}
