#ifndef CARDLISTMODEL_H
#define CARDLISTMODEL_H

#include <QAbstractListModel>
#include <QList>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include "card.h"
//#include "cardfetcher.h"

class CardListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit CardListModel(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    QPixmap pixmap(const QModelIndex & index);

    void addCard(Card* card);
    void removeCharacter(QString characterName);

    Card* getCard(const QModelIndex & index);
    Card* getCardByName(QString cardname);

private:

    QList<Card*>* cardlist = nullptr;
    //CardFetcher cardFetcher;
};

#endif // CARDLISTMODEL_H
