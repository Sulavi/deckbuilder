#ifndef GAMETABLE_H
#define GAMETABLE_H

#include <QMainWindow>
#include <QList>
#include <QFile>
#include <QSharedPointer>
#include <QPushButton>
#include <QMessageBox>

#include "google/protobuf/util/json_util.h"

#include "Decks/deck.h"
#include "Decks/localdeck.h"
#include "Decks/remotedeck.h"
#include "Views/deckview.h"
#include "Views/handview.h"
#include "deckbuilder.pb.h"
#include "deckconfig.hpp"
#include "subwindowhidden.hpp"

#define QSP QSharedPointer

namespace Ui {
class GameTable;
}

class GameTable : public QMainWindow
{
    Q_OBJECT

public:
    explicit GameTable(DeckConfig * config, QWidget *parent = nullptr);
    ~GameTable();

    void addDeck(db::Deck newDeck);
    void stitchDecks();

private:
    Ui::GameTable *ui;
    MODE clientMode;

    NetworkClient * client = nullptr;
    NetworkServer * server = nullptr;

    QList<QSP<Deck>> decks;

private slots:
    void errorPopup(QString err);
};

#endif // GAMETABLE_H
