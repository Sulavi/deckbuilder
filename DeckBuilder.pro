QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

LIBS += -lprotobuf -pthread

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Decks/localdeck.cpp \
    Decks/remotedeck.cpp \
    Network/networkserver.cpp \
    Views/cardview.cpp \
    Views/deckview.cpp \
    Views/handview.cpp \
    Network/networkclient.cpp \
    card.cpp \
    cardfetcher.cpp \
    cardlistmodel.cpp \
    deckbuilder.pb.cc \
    gametable.cpp \
    loadfile.cpp \
    main.cpp \
    mainwindow.cpp \
    resizeablebutton.cpp \
    resizeablelabel.cpp \
    setup.cpp \
    store.cpp

HEADERS += \
    Decks/deck.h \
    Decks/localdeck.h \
    Decks/remotedeck.h \
    Network/networkinterface.h \
    Network/networkserver.h \
    Views/cardview.h \
    Views/deckview.h \
    Views/handview.h \
    Views/view.h \
    Network/networkclient.h \
    card.h \
    cardfetcher.h \
    cardlistmodel.h \
    deckbuilder.pb.h \
    deckconfig.hpp \
    gametable.h \
    loadfile.h \
    mainwindow.h \
    resizeablebutton.h \
    resizeablelabel.h \
    setup.h \
    store.h \
    subwindowhidden.hpp

FORMS += \
    Views/cardview.ui \
    Views/deckview.ui \
    Views/handview.ui \
    gametable.ui \
    loadfile.ui \
    mainwindow.ui \
    setup.ui \
    store.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    Json/Legendary_AntMan.json \
    Json/Legendary_Basic.json \
    Json/Legendary_CaptainAmericaAnniversary.json \
    Json/Legendary_Champions.json \
    Json/Legendary_CivilWar.json \
    Json/Legendary_Core.json \
    Json/Legendary_DarkCity.json \
    Json/Legendary_Deadpool.json \
    Json/Legendary_Dimensions.json \
    Json/Legendary_FantasticFour.json \
    Json/Legendary_Guardians.json \
    Json/Legendary_HeroesOfAsgard.json \
    Json/Legendary_IntoTheCosmos.json \
    Json/Legendary_NewMutants.json \
    Json/Legendary_Noir.json \
    Json/Legendary_PaintTheTownRed.json \
    Json/Legendary_Revelations.json \
    Json/Legendary_SecretWars_1.json \
    Json/Legendary_SecretWars_2.json \
    Json/Legendary_Shield.json \
    Json/Legendary_Spider-Man_Homecoming.json \
    Json/Legendary_Venom.json \
    Json/Legendary_Villains.json \
    Json/Legendary_Villains_FearItself.json \
    Json/Legendary_WorldWarHulk.json \
    Json/Legendary_X-Men.json \
    Schemas/Legendary.yaml \
    deckbuilder.proto

