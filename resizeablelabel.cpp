#include "resizeablelabel.h"

ResizeableLabel::ResizeableLabel(QWidget* parent)
    : QLabel(parent)
{

}

void ResizeableLabel::resizeEvent(QResizeEvent *event){
    int w = width();
    int h = height();

    int maxHeight = w/aspectRatio;
    int maxWidth = h*aspectRatio;

    if(h > maxHeight+1){
        resize(w, maxHeight);
    } else if(w > maxWidth+1){
        resize(maxWidth, h);
    }
}

void ResizeableLabel::setLandscape(bool landscape){
    if(landscape){
        aspectRatio = 1.39;
    } else {
        aspectRatio = 0.719;
    }
}
