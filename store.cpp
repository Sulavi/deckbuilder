#include "store.h"
#include "ui_store.h"

Store::Store(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Store)
{
    ui->setupUi(this);

    this->setWindowTitle("HQ");

    cardList = new QList<Card*>();

    // Context Menu
    QRegularExpression re("card_\\d+");
    QList<QPushButton*> buttons = ui->frame->findChildren<QPushButton*>(re);
    for(int i=0; i<buttons.size(); i++){
        buttons.at(i)->setContextMenuPolicy(Qt::CustomContextMenu);
        connect(buttons.at(i), &QPushButton::customContextMenuRequested, this, &Store::customContextMenuRequested);
        connect(buttons.at(i), &QPushButton::clicked, this, &Store::clicked);
    }

    contextMenu = new QMenu(this);

    QAction* buyAction = new QAction("Buy", this);
    connect(buyAction, &QAction::triggered, [=](){
        buy(contextCardIndex);
    });
    contextMenu->addAction(buyAction);

    QAction* removeAction = new QAction("Remove", this);
    connect(removeAction, &QAction::triggered, [=](){
        remove(contextCardIndex);
    });
    contextMenu->addAction(removeAction);

    QAction* zoomAction = new QAction("Zoom", this);
    connect(zoomAction, &QAction::triggered, [=](){
        popout(contextCardIndex);
    });
    contextMenu->addAction(zoomAction);
}

Store::~Store()
{
    delete ui;
}

void Store::recruitChanged(int recruit){
    this->recruit = recruit;
    ui->recruitValueLabel->setNum(recruit);
}

void Store::setLimitRecruit(bool limit){
    this->limit = limit;
}

void Store::updateStore(){
    for(int i=1; i<=5; i++){
        QString buttonName = "card_" + QString::number(i);
        QPushButton* cardButton = ui->frame->findChild<QPushButton*>(buttonName);

        if(i>cardList->size()){
            QPixmap emptyPixmap = QPixmap(1280, 1780);
            emptyPixmap.fill(QColor(0,0,0,0));
            cardButton->setIcon(emptyPixmap);
        } else {
            cardButton->setIcon(cardList->at(i-1)->getPixmap(false));
        }
    }
}

void Store::customContextMenuRequested(QPoint point){
    QPushButton* button = qobject_cast<QPushButton*>(sender());

    QString buttonName = button->objectName();
    QRegularExpression reg("card_(\\d+)");
    QRegularExpressionMatch match = reg.match(buttonName);
    if(!match.hasMatch()){
        return;
    }

    int cardIndex = match.captured(1).toInt();
    contextCardIndex = cardIndex;
    contextMenu->popup(button->mapToGlobal(point));
}

//Card* Store::takeCard(int index){
//    if(index >= cardList->size()){
//        return nullptr;
//    }

//    Card* card = cardList->takeAt(index-1);

//    return card;
//}

void Store::buy(int index){
    if(index > cardList->size()){
        return;
    }

    if(limit && cardList->at(index-1)->getCost() > recruit){
        QMessageBox msgBox(this);
        msgBox.setText("Not enough Recruit");
        msgBox.exec();
        return;
    }

    Card* card = cardList->takeAt(index-1);

//    recruit -= card->getCost();
//    ui->recruitValueLabel->setNum(recruit);

    updateStore();

    emit CardBought(card);
}

void Store::remove(int index){
    if(index > cardList->size()){
        return;
    }

    cardList->removeAt(index-1);

    updateStore();
}

void Store::popout(int index){
    if(index > cardList->size()){
        return;
    }

    emit Popout(cardList->at(index-1));
}

void Store::clicked(){
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    QRegularExpression re("card_(\\d+)");
    QRegularExpressionMatch match = re.match(button->objectName());
    if(!match.hasMatch()){
        return;
    }

    int cardIndex = match.captured(1).toInt();
    popout(cardIndex);
}

bool Store::addCard(Card* card){
    if(cardList->size() >= 5){
        return false;
    }

    cardList->append(card);

    updateStore();

    return true;
}

void Store::clear(){
    for(int i=0; i<5; i++){
        remove(1);
    }
}
