#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QFile>
#include <QItemSelectionModel>
#include <QDebug>
#include <QDialog>
#include <QQueue>
#include <QRandomGenerator>
#include <QMessageBox>
#include <QSortFilterProxyModel>
#include "card.h"
#include "cardfetcher.h"
#include "cardlistmodel.h"
#include "loadfile.h"
#include "Views/deckview.h"
#include "Views/cardview.h"
#include "store.h"
//#include "Views/handview.h"
#include "Decks/deck.h"
#include "Decks/localdeck.h"
#include "deckconfig.hpp"

#define HANDSIZE 12

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr, DeckConfig * config = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    DeckConfig * config;

    int recruit = 0;
    int attack = 0;

//    QList<Card*> hand;

    CardListModel* cardListModel;
    QSortFilterProxyModel* filterModel;

    LoadFile* loadCardsDialog;
    QList<View*> decks;
    DeckView* discard;
    DeckView* deck;
    DeckView* ko;
//    HandView* hand;

    Store* store;

    QRandomGenerator rand;

    int contextCardIndex;
    QMenu* contextMenu;

private slots:

        void openLoadCardsDialog();

//        bool setCard(Card* card, int index);
//        bool clearCard(int index);

        void on_cardPreview_clicked();

public slots:

//        void cardClicked();

        bool startGame();
        void endTurn();

        void shuffle();
        bool draw(int amount = 1);
//        void drawSpecific(Card* card);
//        bool discardCard(int handIndex);
//        bool playCard(int handIndex);
//        bool koCard(int handIndex);
//        bool cardToDeck(int handIndex, bool front);
        void discardHand();

        void addCard(Card* card);
        bool buyCard(Card* card);

//        void customContextMenuRequested(QPoint);

        void popout(Card* card);

};
#endif // MAINWINDOW_H
