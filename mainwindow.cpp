#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent, DeckConfig * config)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , config(config)
{
    ui->setupUi(this);

    // Card List Model
    cardListModel = new CardListModel(this);
    filterModel = new QSortFilterProxyModel(this);
    filterModel->setSourceModel(cardListModel);
    ui->cardList->setModel(filterModel);

    connect(ui->cardList->selectionModel(), &QItemSelectionModel::selectionChanged, [=](const QItemSelection & selection){
        if(selection.indexes().size() > 0){
            QPixmap selectedPixmap = filterModel->data(selection.indexes().first(), Qt::UserRole).value<QPixmap>();
            ui->cardPreview->setIcon(selectedPixmap);
        } else {
            ui->cardPreview->setIcon(QIcon());
        }
    });

    // Filtering
    connect(ui->searchBar, &QLineEdit::textChanged, [=](QString text){
        filterModel->setFilterRegExp(QRegExp(text, Qt::CaseInsensitive, QRegExp::FixedString));
    });

    // Deck Views
//    discard = new DeckView("Discard", new LocalDeck("Fix me 2"), this);
//    decks.append(discard);
//    deck = new DeckView("Deck", new LocalDeck("Fix me 3", true, true), this);
//    deck->setVerb("Add to Deck");
//    decks.append(deck);
//    ko = new DeckView("KO",new LocalDeck("Fix me 4"), this);
//    decks.append(ko);

//    hand = ui->handWidget;
//    decks.append(hand);
//    hand->setVerb("Add to Hand");

//    discard->setDecks({hand, deck, ko});
//    deck->setDecks({hand, discard, ko});
//    ko->setDecks({hand, deck, discard});
//    hand->setDecks({deck, discard, ko});

    connect(discard, &DeckView::SizeChanged, [=](int newSize){
        ui->discardSizeLabel->setNum(newSize);
    });
    connect(deck, &DeckView::SizeChanged, [=](int newSize){
        ui->deckSizeLabel->setNum(newSize);
    });
    connect(ko, &DeckView::SizeChanged, [=](int newSize){
        ui->koSizeLabel->setNum(newSize);
    });

    // Store
    store = new Store(this);
    ui->storeButton->setVisible(false);
    connect(ui->actionShow_Store, &QAction::toggled, [=](bool checked){
        if(checked){
            store->show();
            ui->storeButton->setVisible(true);
        } else {
            store->close();
            ui->storeButton->setVisible(false);
        }
    });
    connect(ui->storeButton, &QPushButton::clicked, [=](){
        store->show();
    });
    connect(ui->storeButton, &QPushButton::pressed, [=](){
       filterModel->setFilterRegExp(QRegExp("", Qt::CaseInsensitive, QRegExp::FixedString));
       Card* selectedCard = cardListModel->getCard(ui->cardList->selectionModel()->currentIndex());
       filterModel->setFilterRegExp(QRegExp(ui->searchBar->text(), Qt::CaseInsensitive, QRegExp::FixedString));
       store->addCard(selectedCard);
    });
    connect(store, &Store::CardBought, this, &MainWindow::buyCard);
    connect(store, &Store::Popout, this, &MainWindow::popout);


    // Load Cards Dialog
    loadCardsDialog = new LoadFile(this);
    connect(ui->actionLoad_Card_Set, &QAction::triggered, this, &MainWindow::openLoadCardsDialog);
    connect(loadCardsDialog, &LoadFile::CardLoaded, cardListModel, &CardListModel::addCard);
    connect(loadCardsDialog, &LoadFile::CharacterUnchecked, cardListModel, &CardListModel::removeCharacter);

    // Random Generator
    rand = QRandomGenerator::securelySeeded();

    // Menu Actions
    connect(ui->actionRestart, &QAction::triggered, this, &MainWindow::startGame);
    connect(ui->actionShuffle, &QAction::triggered, this, &MainWindow::shuffle);
    connect(ui->actionForceRecruit, &QAction::toggled, store, &Store::setLimitRecruit);
    connect(ui->actionAbout, &QAction::triggered, [=](){
        QMessageBox aboutMessage(this);
        aboutMessage.setText("Deck Builder \n"
                             "Version 0.2\n\n"
                             "This program is used for playing the Legendary Deck Builder board "
                             "game remotely. Players can use Deck Builder to manage their own "
                             "hands while the host uses the physical board game to play the "
                             "Villains.\n\n Cards are loaded in with JSON files that describe "
                             "the playable cards. Deck Builder will automatically load the "
                             "default Strike and Recruit values printed on the card. Any "
                             "additional effects, including Strike and Recruit bonuses, must be "
                             "completed by the player manually.");
        aboutMessage.exec();
    });

    // Connect Buttons
    connect(ui->drawButton, &QPushButton::pressed, [=](){
        draw(1);
    });

    connect(ui->buyCardButton, &QPushButton::pressed, [=](){
       filterModel->setFilterRegExp(QRegExp("", Qt::CaseInsensitive, QRegExp::FixedString));
       Card* selectedCard = cardListModel->getCard(ui->cardList->selectionModel()->currentIndex());
       filterModel->setFilterRegExp(QRegExp(ui->searchBar->text(), Qt::CaseInsensitive, QRegExp::FixedString));
       buyCard(selectedCard);
    });

    connect(ui->endTurnButton, &QPushButton::pressed, this, &MainWindow::endTurn);

//    QRegularExpression re("card_\\d+");
//    QList<QPushButton*> buttons = ui->centralwidget->findChildren<QPushButton*>(re);
//    for(int i=0; i<buttons.size(); i++){
//        connect(buttons.at(i), &QPushButton::pressed, this, &MainWindow::cardClicked);

//        // Context Menu
//        buttons.at(i)->setContextMenuPolicy(Qt::CustomContextMenu);
//        connect(buttons.at(i), &QPushButton::customContextMenuRequested, this, &MainWindow::customContextMenuRequested);
//    }

    connect(ui->addRecruitButton, &QToolButton::pressed, [=](){
        recruit++;
        ui->recruitValueLabel->setNum(recruit);
        store->recruitChanged(recruit);
    });
    connect(ui->removeRecruitButton, &QToolButton::pressed, [=](){
        recruit--;
        ui->recruitValueLabel->setNum(recruit);
        store->recruitChanged(recruit);
    });
    connect(ui->addAttackButton, &QToolButton::pressed, [=](){
        attack++;
        ui->attackValueLabel->setNum(attack);
    });
    connect(ui->removeAttackButton, &QToolButton::pressed, [=](){
        attack--;
        ui->attackValueLabel->setNum(attack);
    });

    connect(ui->showDeckButton, &QToolButton::clicked, [=](){
        deck->show();
    });
    connect(ui->showDiscardButton, &QToolButton::clicked, [=](){
        discard->show();
    });
    connect(ui->showKoButton, &QToolButton::clicked, [=](){
        ko->show();
    });

    //    //Context Menu
    //    contextMenu = new QMenu(this);
    //    QAction* playAction = new QAction("Play", this);
    //    connect(playAction, &QAction::triggered, [=](){
    //        playCard(contextCardIndex);
    //    });
    //    contextMenu->addAction(playAction);

    //    QAction* discardAction = new QAction("Discard", this);
    //    connect(discardAction, &QAction::triggered, [=](){
    //        discardCard(contextCardIndex);
    //    });
    //    contextMenu->addAction(discardAction);

    //    QAction* koAction = new QAction("KO", this);
    //    connect(koAction, &QAction::triggered, [=](){
    //        koCard(contextCardIndex);
    //    });
    //    contextMenu->addAction(koAction);

    //    QMenu* addToDeckMenu = new QMenu("Add to Deck", this);
    //    QAction* topDeckAction = new QAction("Top", addToDeckMenu);
    //    connect(topDeckAction, &QAction::triggered, [=](){
    //        cardToDeck(contextCardIndex, true);
    //    });
    //    QAction* bottomDeckAction = new QAction("Bottom", addToDeckMenu);
    //    connect(bottomDeckAction, &QAction::triggered, [=](){
    //        cardToDeck(contextCardIndex, false);
    //    });
    //    contextMenu->addMenu(addToDeckMenu);
    //    addToDeckMenu->addAction(topDeckAction);
    //    addToDeckMenu->addAction(bottomDeckAction);

    //    QAction* zoomAction = new QAction("Zoom", this);
    //    connect(zoomAction, &QAction::triggered, [=](){
    //        popout(hand.at(contextCardIndex-1));
    //    });
    //    contextMenu->addAction(zoomAction);

    //    //Clear all cards set set blank Pixmaps
    //    for(int i=1; i<=8; i++){
    //        clearCard(i);
    //    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

//void MainWindow::cardClicked(){
//    QPushButton* button = qobject_cast<QPushButton*>(sender());

//    QString buttonName = button->objectName();
//    QRegularExpression reg("card_(\\d+)");
//    QRegularExpressionMatch match = reg.match(buttonName);
//    if(!match.hasMatch()){
//        return;
//    }

//    int cardIndex = match.captured(1).toInt();

//    if(cardIndex <= hand.size()){
//        playCard(cardIndex);
//    }

//}

void MainWindow::openLoadCardsDialog(){
    loadCardsDialog->open();
}

//bool MainWindow::setCard(Card* card, int index){
//    if(index > HANDSIZE){
//        return false;
//    }

//    QString buttonName = "card_" + QString::number(index);

//    QPushButton* cardButton = ui->centralwidget->findChild<QPushButton*>(buttonName);
//    cardButton->setIcon(card->getPixmap(false));

//    return true;
//}

//bool MainWindow::clearCard(int index){
//    if(index > HANDSIZE){
//        return false;
//    }

//    QString buttonName = "card_" + QString::number(index);

//    QPushButton* cardButton = ui->centralwidget->findChild<QPushButton*>(buttonName);
//    QPixmap emptyPixmap = QPixmap(1280, 1780);
//    emptyPixmap.fill(QColor(0,0,0,0));
//    cardButton->setIcon(emptyPixmap);

//    return true;
//}

void MainWindow::shuffle(){
    QList<Card*> tempList = discard->empty();
    QList<Card*> shuffledList;

    while(tempList.size() > 0){
        int index = rand.bounded(0, tempList.size());
        shuffledList.append(tempList.takeAt(index));
    }

    deck->putCards(shuffledList);
}

bool MainWindow::draw(int amount){
    for(int i=0; i<amount; i++){
        if(deck->getSize() <= 0){
            shuffle();
        }

        if(deck->getSize() <= 0){
            return false;
        }

//        //int handIndex = hand.size() + 1;
//        Card* newCard = deck->getCard();
//        hand.append(newCard);
//        setCard(newCard, handIndex);

//        hand->putCard(deck->getCard());
    }



    return true;
}

//void MainWindow::drawSpecific(Card* card){
//    int handIndex = hand.size() + 1;
//    hand.append(card);
//    setCard(card, handIndex);
//}

//bool MainWindow::discardCard(int handIndex){
//    if(handIndex > hand.size()){
//        return false;
//    }

//    discard->putCard(hand.takeAt(handIndex-1));

//    for(int i=handIndex; i<=hand.size(); i++){
//        setCard(hand.at(i-1), i);
//    }

//    clearCard(hand.size()+1);

//    return true;
//}

//bool MainWindow::playCard(int handIndex){

//    Card* card = hand.at(handIndex-1);
//    recruit += card->getRecruit();
//    attack += card->getAttack();
//    ui->recruitValueLabel->setNum(recruit);
//    store->recruitChanged(recruit);
//    ui->attackValueLabel->setNum(attack);

//    return discardCard(handIndex);
//}

//bool MainWindow::koCard(int handIndex){
//    if(handIndex > hand.size()){
//        return false;
//    }

//    ko->putCard(hand.takeAt(handIndex-1));

//    for(int i=handIndex; i<=hand.size(); i++){
//        setCard(hand.at(i-1), i);
//    }

//    clearCard(hand.size()+1);

//    return true;
//}

//bool MainWindow::cardToDeck(int handIndex, bool front){
//    if(handIndex > hand.size()){
//        return false;
//    }

//    deck->putCard(hand.takeAt(handIndex-1), front);

//    for(int i=handIndex; i<=hand.size(); i++){
//        setCard(hand.at(i-1), i);
//    }

//    clearCard(hand.size()+1);

//    return true;
//}

void MainWindow::discardHand(){
//    for(int i=1; i<=hand.size(); i++){
//        clearCard(i);
//    }

//    while(hand.size() > 0){
//        discard->putCard(hand.takeFirst());
//    }

//    hand.clear();
//    discard->putCards(hand->empty());
}

void MainWindow::addCard(Card* card){
    discard->putCard(card);
}

bool MainWindow::buyCard(Card* card){
    if(card == nullptr){
        return false;
    }

    if(ui->actionForceRecruit->isChecked() && recruit < card->getCost()){
        QMessageBox msgBox(this);
        msgBox.setText("Not enough Recruit");
        msgBox.exec();
        return false;
    }

    recruit -= card->getCost();
    ui->recruitValueLabel->setNum(recruit);
    store->recruitChanged(recruit);
    discard->putCard(card);
    return true;
}

bool MainWindow::startGame(){
    Card* agent;
    Card* trooper;

    if(ui->actionUse_Hydra->isChecked()){
        agent = cardListModel->getCardByName("Hydra Operative");
        trooper = cardListModel->getCardByName("Hydra Soldier");
    } else {
        agent = cardListModel->getCardByName("S.H.I.E.L.D. Agent");
        trooper = cardListModel->getCardByName("S.H.I.E.L.D. Trooper");
    }


    if(agent == nullptr || trooper == nullptr){
        QMessageBox msgBox(this);
        msgBox.setText("You must load the basic set before starting a game");
        msgBox.exec();

        return false;
    }

    recruit = 0;
    ui->recruitValueLabel->setNum(recruit);
    store->recruitChanged(recruit);
    attack = 0;
    ui->attackValueLabel->setNum(attack);
    discardHand();
    for(auto it=decks.begin(); it!=decks.end(); it++){
        (*it)->empty();
    }
    store->clear();

    for(int i=0; i<4; i++){
        addCard(trooper);
    }

    for(int i=0; i<8; i++){
        addCard(agent);
    }

    draw(6);
    return true;
}

void MainWindow::endTurn(){
    discardHand();
    draw(6);
    recruit = 0;
    attack = 0;
    ui->recruitValueLabel->setNum(recruit);
    store->recruitChanged(recruit);
    ui->attackValueLabel->setNum(attack);
}

//void MainWindow::customContextMenuRequested(QPoint point){
//    QPushButton* button = qobject_cast<QPushButton*>(sender());

//    QString buttonName = button->objectName();
//    QRegularExpression reg("card_(\\d+)");
//    QRegularExpressionMatch match = reg.match(buttonName);
//    if(!match.hasMatch()){
//        return;
//    }

//    int cardIndex = match.captured(1).toInt();
//    if(cardIndex <= hand.size()){
//        contextCardIndex = cardIndex;
//        contextMenu->popup(button->mapToGlobal(point));
//    }
//}

void MainWindow::popout(Card* card){
    CardView* cardView = new CardView(card, this);

    connect(cardView, &CardView::finished, [=](){
        cardView->deleteLater();
    });

    cardView->show();
}

void MainWindow::on_cardPreview_clicked()
{
    filterModel->setFilterRegExp(QRegExp("", Qt::CaseInsensitive, QRegExp::FixedString));
    Card* selectedCard = cardListModel->getCard(ui->cardList->selectionModel()->currentIndex());
    filterModel->setFilterRegExp(QRegExp(ui->searchBar->text(), Qt::CaseInsensitive, QRegExp::FixedString));
    if(selectedCard != nullptr){
        popout(selectedCard);
    }
}
