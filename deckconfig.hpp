#ifndef DECKCONFIG_HPP
#define DECKCONFIG_HPP

#include <string>

enum MODE {
    LOCAL = 0,
    SERVER = 1,
    CLIENT = 2
};

typedef struct {
    bool valid = false;
    MODE mode;
    std::string configPath;
    std::string decksPath;
    bool useCache;
    std::string cachePath;
    std::string ip;
    unsigned int port;
} DeckConfig;

#endif // DECKCONFIG_HPP
