#ifndef CARD_H
#define CARD_H

#include <QObject>
#include <QUrl>
#include <QPixmap>
#include <QDebug>

#include <QNetworkAccessManager>
#include <QNetworkRequest>

class Card : public QObject
{
    Q_OBJECT

public:
    explicit Card(QString name,
                  QString hero,
                  QString team,
                  QString cardclass,
                  int cost,
                  int attack,
                  int recruit,
                  QString url,
                  QString text = "",
                  bool split = false,
                  QObject *parent = nullptr);



    QString getName();
    QString getHero();
    QUrl getUrl();
    QString getTeam();
    QString getClass();
    int getCost();
    int getAttack();
    int getRecruit();
    QString getText();
    bool getSplit();

    QPixmap getPixmap(bool scaled = true);
    void setPixmap(QPixmap pixmap);

private:
    QString cardname;
    QString heroname;
    QUrl url;
    QString team;
    QString cardClass;
    int cost;
    int attack;
    int recruit;
    QString text;
    bool split;

    QPixmap pixmap;


signals:
    void pictureUpdated();
};

#endif // CARD_H
