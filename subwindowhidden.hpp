#ifndef SUBWINDOWHIDDEN_H
#define SUBWINDOWHIDDEN_H

#include <QMdiSubWindow>
#include <QCloseEvent>

class SubWindowHidden : public QMdiSubWindow
{
    void closeEvent(QCloseEvent * closeEvent) override {
        this->hide();
        closeEvent->ignore();
    }
};

#endif // SUBWINDOWHIDDEN_H
