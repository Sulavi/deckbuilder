#include "localdeck.h"

LocalDeck::LocalDeck(std::string index, bool hidden, bool ordered)
{
    this->index = index;
    this->hidden = hidden;
    this->ordered = ordered;
}

LocalDeck::~LocalDeck(){

}

void LocalDeck::addCard(Card *card, int index){
    cardList.insert(index, card);
}

void LocalDeck::addCard(Card *card){
    cardList.append(card);
}

Card* LocalDeck::removeCard(int index){
    return cardList.takeAt(index);
}

Card* LocalDeck::getCard(int index){
    return cardList.at(index);
}

QList<Card*> LocalDeck::clear(){
    QList<Card*> cards = cardList;
    cardList.clear();
    return cards;
}
