#ifndef DECK_H
#define DECK_H

#include <QObject>
#include <QList>
#include <QMdiSubWindow>
#include "card.h"
#include "deckbuilder.pb.h"

class Deck{
public:
    virtual ~Deck(){}

    void setWindow(QMdiSubWindow * window){ this->window = window; }
    QMdiSubWindow * getWindow(){ return window; }

    void setView(db::VIEW view){ this->view = view; }
    std::string getIndex(){ return index; }
    void setHidden(bool hidden){ this->hidden = hidden; }
    bool isHidden(){ return hidden; }
    void setOrdered(bool ordered){ this->ordered = ordered; }
    bool isOrdered(){ return ordered; }
    int size(){ return cardList.size(); }

    virtual void addCard(Card* card, int index) = 0;
    virtual void addCard(Card* card) = 0;
    virtual Card* removeCard(int index) = 0;
    virtual Card* getCard(int index) = 0;
    virtual QList<Card*> clear() = 0;                   // Returns the current list of cards

protected:
    QList<Card*> cardList;
    db::VIEW view;
    QMdiSubWindow * window = nullptr;

    std::string index;
    bool hidden;
    bool ordered;
};

#endif // DECK_H
