#ifndef LOCALDECK_H
#define LOCALDECK_H

#include "Decks/deck.h"

class LocalDeck : public Deck
{
public:
    LocalDeck(std::string index, bool hidden = false, bool ordered = false);
    ~LocalDeck() override;

private:
//    QList<Card*> cardList;

//    std::string index; // Unique deck identifier
//    bool hidden;
//    bool ordered;

public:
    void addCard(Card* card, int index) override;
    void addCard(Card* card) override;
    Card* removeCard(int index) override;
    Card* getCard(int index) override;
    QList<Card*> clear() override;
};

#endif // LOCALDECK_H
