#ifndef REMOTEDECK_H
#define REMOTEDECK_H

#include <QList>

#include "Decks/deck.h"
#include "Network/networkclient.h"
#include "Network/networkserver.h"

class RemoteDeck : public Deck
{
public:
    RemoteDeck(std::string index, NetworkClient * client);
    RemoteDeck(std::string index, NetworkServer * server);
    ~RemoteDeck() override;

private:
//    QList<Card*> cardList;

    NetworkClient * client = nullptr;
    NetworkServer * server = nullptr;
    bool isServer;

public:
    void addCard(Card* card, int index) override;
    void addCard(Card* card) override;
    Card* removeCard(int index) override;
    Card* getCard(int index) override;
    QList<Card*> clear() override;
};

#endif // REMOTEDECK_H
