#ifndef CARDVIEW_H
#define CARDVIEW_H

#include <QDialog>
#include "card.h"

namespace Ui {
class CardView;
}

class CardView : public QDialog
{
    Q_OBJECT

public:
    explicit CardView(Card* card, QWidget *parent = nullptr);
    ~CardView();

private:
    Ui::CardView *ui;
    Card* card;
};

#endif // CARDVIEW_H
