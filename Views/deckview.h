#ifndef DECKVIEW_H
#define DECKVIEW_H

#include <QDialog>
#include <QList>
#include <QString>
#include <QMenu>
#include <QRegularExpression>
#include <QSharedPointer>
#include <QCloseEvent>
#include "../Decks/deck.h"
#include "../Views/view.h"
#include "card.h"
#include "cardview.h"

namespace Ui {
class DeckView;
}

class DeckView : public QDialog, public View
{
    Q_OBJECT

public:
    explicit DeckView(QSharedPointer<Deck> deck, QWidget *parent = nullptr);
    ~DeckView();

private:
    Ui::DeckView *ui;
    QMenu* contextMenu;

    // Name of the deck
    QString name;

    // Action to move cards to this deck
    // Cosmetic and Optional
    QString verb;

//    // True if the cards are hidden by default
//    bool hidden;

//    // When true, give users the option to put cards in specific spots
//    bool ordered;

    // Cards in deck
//    QList<Card*>* cardList;
    QSharedPointer<Deck> deck;

    // List of decks this deck can pass cards to
    QList<View*> decks;

    int currentPage = 1;

    int cardsInPage();

    bool setCard(Card* card, int inPageIndex);
    bool clearCard(int inPageIndex);
    void updateCurrentPage();
    Card* takeCard(int cardIndex, int page);

    int contextCardIndex;
    void customContextMenuRequested(QPoint);

    void reveal(int inPageIndex);
    void zoom(int inPageIndex);

    void scrollRight();
    void scrollLeft();

//    void closeEvent(QCloseEvent * event) override;

signals:
    void Popout(Card* card);
    void SizeChanged(int newSize);

public slots:
    // Set list of decks this deck may pass cards to.
    // All Decks must be initialized before calling this function
    void setDecks(QList<View*> decks) override;

    QString getName() override;
    bool isOrdered() override;

    QString getVerb() override;
    void setVerb(QString verb) override;

    int getSize() override;
    void clear();
    Card* getCard() override;
    bool putCard(Card* card, int index = -1) override;
    void putCards(QList<Card*> cards) override;
    QList<Card*> empty() override;
};

#endif // DECKVIEW_H
