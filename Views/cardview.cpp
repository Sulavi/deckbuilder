#include "cardview.h"
#include "ui_cardview.h"

CardView::CardView(Card* card, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::CardView)
    , card(card)
{
    ui->setupUi(this);


    ui->label->setPixmap(card->getPixmap(false));
    ui->label->setScaledContents(true);
    ui->label->setLandscape(card->getSplit());

    ui->cardNameLabel->setText(card->getName());
    ui->attackValueLabel->setNum(card->getAttack());
    ui->costValueLabel->setNum(card->getCost());
    ui->recruitValueLabel->setNum(card->getRecruit());
    ui->teamLabel->setText(card->getTeam());
    ui->cardTextLabel->setText(card->getText());

    ui->cardNameLabel->setVisible(false);
    ui->attackValueLabel->setVisible(false);
    ui->costValueLabel->setVisible(false);
    ui->recruitValueLabel->setVisible(false);
    ui->teamLabel->setVisible(false);
    ui->cardTextLabel->setVisible(false);
    ui->attackLabel->setVisible(false);
    ui->recruitLabel->setVisible(false);
    ui->costLabel->setVisible(false);

    this->setWindowTitle(card->getName());

    if(card->getSplit()){
        this->resize(712, 512);
    } else {
        this->resize(512, 712);
    }
}

CardView::~CardView()
{
    delete ui;
}
