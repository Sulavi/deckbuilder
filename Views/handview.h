#ifndef HANDVIEW_H
#define HANDVIEW_H

#include <QWidget>
#include <QList>
#include <QMenu>
#include <QRegularExpression>
#include <QSharedDataPointer>
#include "../Decks/deck.h"
#include "../Decks/localdeck.h"
#include "../Views/view.h"
#include "resizeablebutton.h"
#include "cardview.h"

namespace Ui {
class HandView;
}

class HandView : public QWidget, public View
{
    Q_OBJECT

public:
    explicit HandView(QSharedPointer<Deck> deck, QWidget *parent = nullptr);
//    explicit HandView(QWidget * parent = nullptr);
    ~HandView();

    void setDecks(QList<View*> decks) override;

    QString getName() override;

    QString getVerb() override;
    void setVerb(QString verb) override;

    int getSize() override;

    bool putCard(Card* card, int index = -1) override;
    void putCards(QList<Card *> cards) override;

    Card * getCard() override;
    QList<Card *> empty() override;

    bool isOrdered() override;

private:
    Ui::HandView *ui;
    QMenu* contextMenu;

    int cardCount;
    //QList<Card*> cardList;
    QSharedPointer<Deck> deck;
    QList<View*> decks;

    QList<ResizeableButton*> buttons;

    bool ordered;

    QString verb;

    int cardIndex;

    Card* takeCard(int index);

    bool setCard(Card* card, int index);
    void updateAfter(int index);

    void zoom(int index);
    void customContextMenuRequested(QPoint point);
};

#endif // HANDVIEW_H
