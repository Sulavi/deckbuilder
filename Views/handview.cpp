#include "handview.h"
#include "ui_handview.h"

HandView::HandView(QSharedPointer<Deck> deck, QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::HandView)
    , cardCount(0)
    , deck(deck)
{
    ui->setupUi(this);
    buttons.append(ui->card1);
    buttons.append(ui->card2);
    buttons.append(ui->card3);
    buttons.append(ui->card4);
    buttons.append(ui->card5);
    buttons.append(ui->card6);
    buttons.append(ui->card7);
    buttons.append(ui->card8);
    buttons.append(ui->card9);
    buttons.append(ui->card10);
    buttons.append(ui->card11);
    buttons.append(ui->card12);
    buttons.append(ui->card13);
    buttons.append(ui->card14);
    buttons.append(ui->card15);
    buttons.append(ui->card16);
    buttons.append(ui->card17);
    buttons.append(ui->card18);
    buttons.append(ui->card19);
    buttons.append(ui->card20);

    for(auto it=buttons.begin(); it!=buttons.end(); it++){
        (*it)->setVisible(false);
        connect((*it), &QPushButton::customContextMenuRequested, this, &HandView::customContextMenuRequested);
    }

    contextMenu = new QMenu();

    // Build Basic Context Menu
    QAction* zoomAction = new QAction("Zoom", this);
    connect(zoomAction, &QAction::triggered, [=](){
        zoom(cardIndex);
    });
    contextMenu->addAction(zoomAction);
}

HandView::~HandView()
{
    delete ui;
}

void HandView::setDecks(QList<View *> decks){
    this->decks = decks;

    for(auto it=decks.begin(); it!=decks.end(); it++){
        View* view = *it;
        QString actionName = view->getVerb() != "" ? view->getVerb() : view->getName();

        if(view->isOrdered()){
            QMenu* moveMenu = new QMenu(actionName, this);
            QAction* topMoveAction = new QAction("Top", moveMenu);
            connect(topMoveAction, &QAction::triggered, [this, view](){
                view->putCard(takeCard(cardIndex), 0);
            });
            QAction* bottomMoveAction = new QAction("Bottom", moveMenu);
            connect(bottomMoveAction, &QAction::triggered, [this, view](){
                view->putCard(takeCard(cardIndex), -1);
            });
            moveMenu->addAction(topMoveAction);
            moveMenu->addAction(bottomMoveAction);
            contextMenu->insertMenu(contextMenu->actions()[0], moveMenu);
        } else {
            QAction* moveAction = new QAction(actionName, this);
            connect(moveAction, &QAction::triggered, [this, view](){
                view->putCard(takeCard(cardIndex));
            });
            contextMenu->insertAction(contextMenu->actions()[0], moveAction);
        }
    }
}

QString HandView::getName() {
    return QString::fromStdString(deck->getIndex());
}

QString HandView::getVerb(){
    return verb;
}

void HandView::setVerb(QString verb){
    this->verb = verb;
}

int HandView::getSize(){
    return deck->size();
}

bool HandView::putCard(Card *card, int index){
    if(index > deck->size() || index > deck->size() - index -1){
        qDebug() << "Failed to putCard, Index outside of range";
        return false;
    }

    if(index < 0){
        index = deck->size() - index -1;
    }
    deck->addCard(card, index);
    cardCount++;

//    while(buttons.size() < cardCount){
//        ResizeableButton* newCard = new ResizeableButton;
//        newCard->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum));
//        newCard->setIconSize(QSize(128, 178));
//        newCard->setMinimumSize(QSize(128, 178));
//        newCard->setMaximumSize(QSize(1280, 1780));
//        newCard->setFlat(true);
//        ui->cardLayout->addWidget(newCard);
//        buttons.append(newCard);
//    }

    setCard(card, index);

    return true;
}

void HandView::putCards(QList<Card*> cards){
    for(Card* card : cards){
        putCard(card);
    }
}

Card* HandView::takeCard(int index){
    cardCount--;
    Card* card = deck->removeCard(index);
    buttons.at(deck->size())->setVisible(false);
    updateAfter(index);
    return card;
}

Card* HandView::getCard(){
    return takeCard(0);
}

bool HandView::isOrdered(){
    return ordered;
}

QList<Card*> HandView::empty(){
    //cardList.clear();

//    for(auto it=buttons.begin(); it!=buttons.end(); it++){
//        ui->cardLayout->removeWidget(*it);
//        delete *it;
//    }
    //buttons.clear();

    cardCount = 0;
    for(int i=0; i<deck->size(); i++){
        buttons.at(i)->setVisible(false);
    }

//    QList<Card*> cards(cardList);
//    cardList.clear();
    return deck->clear();
}

bool HandView::setCard(Card *card, int index){
    buttons.at(index)->setIcon(card->getPixmap(false));
    buttons.at(index)->setVisible(true);
    return true;
}

void HandView::updateAfter(int index){
    for(int i=index; i<deck->size(); i++){
        buttons.at(i)->setIcon(deck->getCard(i)->getPixmap());
    }
}

void HandView::zoom(int index){
    CardView* cardView = new CardView(deck->getCard(index));
    connect(cardView, &CardView::finished, [=](){
        cardView->deleteLater();
    });
    cardView->show();
}

void HandView::customContextMenuRequested(QPoint point){
    qDebug() << "Context menu requested";
    QPushButton* button = qobject_cast<QPushButton*>(sender());

    QString buttonName = button->objectName();
    QRegularExpression reg("card(\\d+)");
    QRegularExpressionMatch match = reg.match(buttonName);
    if(!match.hasMatch()){
        return;
    }

    cardIndex = match.captured(1).toInt()-1;
    contextMenu->popup(button->mapToGlobal(point));
}
