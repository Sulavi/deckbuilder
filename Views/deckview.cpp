#include "deckview.h"
#include "ui_deckview.h"

DeckView::DeckView(QSharedPointer<Deck> deck, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::DeckView)
    , deck(deck)
{
    ui->setupUi(this);

    this->setWindowTitle(QString::fromStdString(deck->getIndex()));

    // Context Menu
    QRegularExpression re("card_\\d+");
    QList<QPushButton*> buttons = ui->frame->findChildren<QPushButton*>(re);
    for(int i=0; i<buttons.size(); i++){
        buttons.at(i)->setContextMenuPolicy(Qt::CustomContextMenu);
        connect(buttons.at(i), &QPushButton::customContextMenuRequested, this, &DeckView::customContextMenuRequested);
    }

    contextMenu = new QMenu(this);

    // Build Basic Context Menu
    QAction* zoomAction = new QAction("Zoom", this);
    connect(zoomAction, &QAction::triggered, [=](){
        zoom(contextCardIndex);
    });
    contextMenu->addAction(zoomAction);

    if(deck->isHidden()){
        QAction* revealAction = new QAction("Hide/Reveal", this);
        connect(revealAction, &QAction::triggered, [=](){
            reveal(contextCardIndex);
        });
        contextMenu->addAction(revealAction);
    }

    //Scroll Buttons
    connect(ui->rightButton, &QToolButton::pressed, this, &DeckView::scrollRight);
    connect(ui->leftButton, &QToolButton::pressed, this, &DeckView::scrollLeft);

    updateCurrentPage();
}

DeckView::~DeckView()
{
    delete ui;
}

void DeckView::setDecks(QList<View*> decks){
    this->decks = decks;

    for(auto it=decks.begin(); it!=decks.end(); it++){
        View* view = *it;
        QString actionName = view->getVerb() != "" ? view->getVerb() : view->getName();

        if(view->isOrdered()){
            QMenu* moveMenu = new QMenu(actionName, this);
            QAction* topMoveAction = new QAction("Top", moveMenu);
            connect(topMoveAction, &QAction::triggered, [this, view](){
                view->putCard(takeCard(contextCardIndex, currentPage), 0);
            });
            QAction* bottomMoveAction = new QAction("Bottom", moveMenu);
            connect(bottomMoveAction, &QAction::triggered, [this, view](){
                view->putCard(takeCard(contextCardIndex, currentPage), -1);
            });
            moveMenu->addAction(topMoveAction);
            moveMenu->addAction(bottomMoveAction);
            contextMenu->insertMenu(contextMenu->actions()[0], moveMenu);
        } else {
            QAction* moveAction = new QAction(actionName, this);
            connect(moveAction, &QAction::triggered, [this, view](){
                view->putCard(takeCard(contextCardIndex, currentPage));
            });
            contextMenu->insertAction(contextMenu->actions()[0], moveAction);
        }
    }
}

QString DeckView::getName(){
    return name;
}

QString DeckView::getVerb(){
    return verb;
}

void DeckView::setVerb(QString verb){
    this->verb = verb;
}

int DeckView::getSize(){
    return deck->size();
}

void DeckView::clear(){
    deck->clear();

    currentPage = 1;
    ui->pageLabel->setNum(currentPage);

    updateCurrentPage();

    emit SizeChanged(0);
}

int DeckView::cardsInPage(){
    int cards = deck->size() - ((currentPage-1)*12);
    //return cards > 12 ? 12 : cards;
    return cards;
}

bool DeckView::setCard(Card *card, int inPageIndex){
    if(inPageIndex > 12){
        return false;
    }

    QString buttonName = "card_" + QString::number(inPageIndex);

    QPushButton* cardButton = ui->frame->findChild<QPushButton*>(buttonName);

    cardButton->setIcon(card->getPixmap(false));

    return true;
}

bool DeckView::clearCard(int inPageIndex){
    if(inPageIndex > 12){
        return false;
    }

    QString buttonName = "card_" + QString::number(inPageIndex);

    QPushButton* cardButton = ui->frame->findChild<QPushButton*>(buttonName);
    QPixmap emptyPixmap = QPixmap(1280, 1780);
    emptyPixmap.fill(QColor(0,0,0,0));
    cardButton->setIcon(emptyPixmap);

    return true;
}

void DeckView::updateCurrentPage(){

    for(int i=1; i<=12; i++){
        int listIndex = ((currentPage-1)*12) + (i-1);
        if(listIndex >= deck->size()){
            clearCard(i);
        } else{
            setCard(deck->getCard(listIndex), i);
        }
    }

    ui->rightButton->setEnabled(cardsInPage() > 12);
    ui->leftButton->setEnabled(currentPage != 1);
}

Card* DeckView::takeCard(int cardIndex, int page){
    int index = cardIndex-1 + ((page-1)*12);

    if(index >= deck->size()){
        return nullptr;
    }

    Card* card = deck->removeCard(index);

    if(page <= currentPage){
        updateCurrentPage();
    }

    emit SizeChanged(deck->size());

    return card;
}

Card* DeckView::getCard(){
    return takeCard(1,1);
}

bool DeckView::putCard(Card* card, int index){

    if(index > deck->size() || (index != 0 && index > deck->size() - index -1)){
        qDebug() << "Failed to putCard, Index outside of range";
        return false;
    }

    if(index>=0){
        deck->addCard(card, index);
    } else {
        deck->addCard(card, deck->size() - index -1);
    }

    updateCurrentPage();
    emit SizeChanged(deck->size());
    return true;
}

void DeckView::putCards(QList<Card*> cards){
    for(auto it=cards.constBegin(); it!=cards.constEnd(); it++){
        deck->addCard(*it);
    }

    updateCurrentPage();
    emit SizeChanged(deck->size());
}

QList<Card*> DeckView::empty(){
    return deck->clear();
}

void DeckView::customContextMenuRequested(QPoint point){
    QPushButton* button = qobject_cast<QPushButton*>(sender());

    QString buttonName = button->objectName();
    QRegularExpression reg("card_(\\d+)");
    QRegularExpressionMatch match = reg.match(buttonName);
    if(!match.hasMatch()){
        return;
    }

    int cardIndex = match.captured(1).toInt();
    if(cardIndex <= cardsInPage()){
        contextCardIndex = cardIndex;
        contextMenu->popup(button->mapToGlobal(point));
    }
}

void DeckView::reveal(int inPageIndex){
    //TODO
}

void DeckView::zoom(int inPageIndex){
    int listIndex = ((currentPage-1)*12) + (inPageIndex-1);

    CardView* cardView = new CardView(deck->getCard(listIndex));
    connect(cardView, &CardView::finished, [=](){
        cardView->deleteLater();
    });
    cardView->show();
}

void DeckView::scrollRight(){
    currentPage++;
    ui->pageLabel->setNum(currentPage);
    updateCurrentPage();
}

void DeckView::scrollLeft(){
    currentPage--;
    ui->pageLabel->setNum(currentPage);
    updateCurrentPage();
}

bool DeckView::isOrdered(){
    return deck->isOrdered();
}

//void DeckView::closeEvent(QCloseEvent * event){
//    event->ignore();
//    this->hide();
//}
