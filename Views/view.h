#ifndef VIEW_H
#define VIEW_H

#include <QObject>
#include <QList>
#include "card.h"

class View
{
public:
    // Set the list of decks  this deck can send cards to
    virtual void setDecks(QList<View*> decks) = 0;

    // Returns the name of the deck
    virtual QString getName() = 0;

    // Returns the string used when sending cards to this deck
    virtual QString getVerb() = 0;
    virtual void setVerb(QString verb) = 0;

    // Returns current number of cards in the deck
    virtual int getSize() = 0;

    // Add or remove a card from the deck
    // Index is from the front, added to back by default
    virtual bool putCard(Card* card, int index = -1) = 0;
    virtual void putCards(QList<Card*> cards) = 0;
    virtual Card* getCard() = 0;

    // Returns whether the deck ir ordered or not.
    // Ordered decks should have options to put a card in any position
    virtual bool isOrdered() = 0;

    // Clears and returns all cards in the deck
    virtual QList<Card*> empty() = 0;

};

#endif // VIEW_H
