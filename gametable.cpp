#include "gametable.h"
#include "ui_gametable.h"

GameTable::GameTable(DeckConfig * config, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GameTable)
{
    ui->setupUi(this);

    this->clientMode = config->mode;

    if(config->mode == SERVER){
        server = new NetworkServer(this);
        server->Listen(config->port);
    } else if(config->mode == CLIENT){
        client = new NetworkClient(this);
        client->Connect(QString::fromStdString(config->ip), config->port);
    }

    // Load Config file
    db::GameDescription gameDescription;
    QFile configFile(QString::fromStdString(config->configPath));
    if(!configFile.open(QIODevice::ReadOnly | QIODevice::Text)){
        errorPopup("Unable to open config file");
        exit(-1);
    }
    auto status = google::protobuf::util::JsonStringToMessage(configFile.readAll().toStdString(), &gameDescription);
    if(status != google::protobuf::util::Status::OK){
        errorPopup("Unable to parse config file");
        exit(-1);
    }
    configFile.close();

    for(db::Deck deck : *gameDescription.mutable_decks()){
        addDeck(deck);
    }

    // TODO: Hook all the decks together
    stitchDecks();
}

GameTable::~GameTable()
{
    delete ui;
}

void GameTable::addDeck(db::Deck newDeck){
    QSP<Deck> deck;

    if(!newDeck.remote()){
        deck = QSP<Deck>(new LocalDeck(newDeck.index(), newDeck.hidden(), newDeck.ordered()));
    } else {
        if(clientMode == SERVER){
            deck = QSP<Deck>(new RemoteDeck(newDeck.index(), server));
        } else {
            deck = QSP<Deck>(new RemoteDeck(newDeck.index(), client));
        }
    }

    deck->setHidden(newDeck.hidden());
    deck->setOrdered(newDeck.ordered());
    deck->setView(newDeck.view());

    decks.push_back(deck);

    // Add deck to sidebar menu
    QPushButton * deckButton = new QPushButton(QString::fromStdString(deck->getIndex()), ui->deckList);
    ui->deckListContents->layout()->addWidget(deckButton);

    // Create View
//    DeckView* view;
    SubWindowHidden * subWindow = new SubWindowHidden;
    if(newDeck.view() == db::VIEW::PILE){
        auto view = new DeckView(deck, ui->table);
        subWindow->setWidget(view);
    } else if(newDeck.view() == db::VIEW::HAND){
        auto view = new HandView(deck, ui->table);
        subWindow->setWidget(view);
    }

    deck->setWindow(subWindow);
    ui->table->addSubWindow(subWindow);

    connect(deckButton, &QPushButton::pressed, this, [=](){
        deck->getWindow()->show();
        ui->table->setActiveSubWindow(deck->getWindow());
    });

}

void GameTable::stitchDecks(){

}

void GameTable::errorPopup(QString msg){
    QMessageBox::warning(this, "Error", msg);
}
